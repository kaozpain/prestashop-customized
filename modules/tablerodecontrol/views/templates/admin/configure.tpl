{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row moduleconfig-header text-center" style="border: double;">
		<h2>{l s='Tablero de control Bi' mod='tablerodecontrol'}</h2>
		<h4>El modulo se basa en un minimo del 0% y un maximo del 100%</h4>
		<h4>Se mostrara datos de solo los ultimos 3 años</h4>
	</div>

	<hr />

	<div class="moduleconfig-content">
		<div class="row" >
			<div class="col-xs-12">
				<p>
					{$isp=$variableSmarty3}
					{$cont=0}
					{foreach name=outer item=variable1 from=$variableSmarty1}
						{foreach key=key item=item from=$variable1}
							{if $cont<3}
							{if {$key}=='YEAR(b.date_add)'}
								<h1 style="border: dashed; font-size: 60px" class="text-center">{$item}</h2>
							{/if}
							{/if}
							{if {$key}=='sum(a.product_quantity)'}
								{foreach item=variable3 from=$isp}
									{foreach key=key2 item=item2 from=$variable3}
										{if $variable1[0]==$variable3[2]}											<div style="border: dashed;">
												{if {$key2}=='name'}
													<h2 class="text-center">{$item2}</h2>
												{/if}
												{if {$key2}=='sum(a.product_quantity)'}
												<div align="center">
													<img src="src/tablerobi.php?var1={$item}&&var2={$item2}"  />
												</div>
													<h4 class="text-center">Ventas totales={$item}</h4>
													<h4 class="text-center">Productos vendidos={$item2}</h4>
													<h4 class="text-center"><strong>Porcentaje={number_format((100*$item2)/$item,2)}</strong></h4>
												{/if}
											</div>
										{/if}
									{/foreach}
								{/foreach}
									<br/>	
								{$cont=$cont+1}
								{if $cont==1}
									{$isp=$variableSmarty4}
								{/if}
								{if $cont==2}
									{$isp=$variableSmarty5}
								{/if}
							{/if}
						{/foreach}
					{/foreach}
				</p>
				<br />
			</div>
		</div>
	</div>
</div>
