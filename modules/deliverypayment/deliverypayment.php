<?php

/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    Hennes HervÃ© <contact@h-hennes.fr>
 *  @copyright 2013-2017 Hennes HervÃ©
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  http://www.h-hennes.fr/blog/
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class DeliveryPayment extends PaymentModule
{
    protected $_html;

    public function __construct()
    {
        $this->author    = 'AM';
        $this->name      = 'deliverypayment';
        $this->tab       = 'payment_gateways';
        $this->version   = '0.1.0';
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Pago en efectivo');
        $this->description = $this->l('Pago en efectivo');
    }

    public function install()
    {
        if (!parent::install() 
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('paymentReturn')
            ) {
            return false;
        }
        return true;
    }

    /**
     * Affichage du paiement dans le checkout
     * PS 17 
     * @param type $params
     * @return type
     */
    public function hookPaymentOptions($params) {

        if (!$this->active) {
            return;
        }

        //Paiement Standard sans passerelle
        $standardPayment = new PaymentOption();
        
        //Inputs supplÃ©mentaires (utilisÃ© idÃ©alement pour des champs cachÃ©s )
        $inputs = [
            [
                'name' => 'custom_hidden_value',
                'type' => 'hidden',
                'value' => '30'
            ],
            [
                'name' => 'id_customer',
                'type' => 'hidden',
                'value' => $this->context->customer->id,
            ],
        ];
        $standardPayment->setModuleName($this->name)
                //Logo de paiement
                //->setLogo($this->context->link->getBaseLink().'/modules/deliverypayment/views/img/logo.png')
                ->setInputs($inputs)
                //->setBinary() UtilisÃ© si une Ã©xÃ©cution de binaire est nÃ©cessaires ( module atos par ex )
                //Texte de description
                ->setCallToActionText($this->l('Pago en efectivo'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
                //Texte informatif supplÃ©mentaire
                ->setAdditionalInformation($this->fetch('module:deliverypayment/views/templates/hook/displayPayment.tpl'));

        return [$standardPayment];
    }

    /**
     * Affichage du message de confirmation de la commande
     * @param type $params
     * @return type
     */
    public function hookDisplayPaymentReturn($params) 
    {
        if (!$this->active) {
            return;
        }
        
        $this->smarty->assign(
            $this->getTemplateVars()
            );
        return $this->fetch('module:deliverypayment/views/templates/hook/payment_return.tpl');
    }

    /**
     * RÃ©cupÃ©ration des informations du template
     * @return array
     */
    public function getTemplateVars()
    {
        return [
            'shop_name' => $this->context->shop->name,
            'custom_var' => $this->l('My custom var value'),
            'payment_details' => $this->l('custom details'),
        ];
    }

}