<?php
    // previne o cache
    header ("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
    header ("Content-Disposition:inline; filename=\"grafico.png\"");

    function rgba($i,$c){
        return imagecolorallocatealpha($i,hexdec("$c[1]$c[2]"),hexdec("$c[3]$c[4]"),hexdec("$c[5]$c[6]"),hexdec("$c[7]$c[8]"));
    }



    $fundo=imagecreatefrompng("velocimetro.png"); // não esquecer de verificar o nome do arquivo
    $seta= imagecreatefrompng("senalero.png");  // não esquecer de verificar o nome do arquivo
    $destino = ImageCreate(240,240); 
    $marca_larg=imagesx($seta);
    $marca_alt= imagesy($seta);
    imageCopyResized($destino, $seta, -10,3,0,0,240,240,$marca_larg,$marca_alt);
    //
    
    $tr=rgba($seta ,"#0000007f");
    // define as configurações da escala
    $max=100; // valor maximo da escala
    $max_an=180; // a abertura maxima em graus
    $min_an=0; // angulo inicial
    // define o centro de rotação do ponteiro
    $pos[0]=160;  // eixo horizontal
    $pos[1]=140;  // eixo vertical

    ////////////////////////
    $v=(100/$_GET['var1'])*$_GET['var2']; // valor de teste
    ////////////////////////

    // calcula o angulo de rotação de acordo com a abertura maxima,
    //angulo inicial e o valor
    $vp=$v*($max_an/100);
    $vp=$vp/($max/100)+$min_an;

    // transforma em numero negativo
    $vp=$vp*(-1);

    $destino=imagerotate($destino,$vp,$tr);
    
    // faz a rotação da seta
    //$seta=imagerotate($seta,$vp,$tr);
    // pega as dimensoes da seta apos a rotacao
    $marca_larg=imagesx($destino);
    $marca_alt= imagesy($destino);
    // calcula o centro da seta apos a rotacao
    $cx=$marca_larg/2;
    $cy=$marca_alt/2;
    //imageCopyResized($destino, $seta, $pos[0]-82,$pos[1]-70,0,0,240,240,$marca_larg,$marca_alt);
    // sobrepoe a seta no fundo
    imagecopyresampled($fundo,$destino,$pos[0]-$cx,$pos[1]-$cy,0,0,$marca_larg,$marca_alt,$marca_larg,$marca_alt);

    // exibe a imagem
    header("Content-type: image/png");
    imagepng($fundo);    
?>