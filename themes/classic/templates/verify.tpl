{extends file='page.tpl'}

{block name='page_header_container'}{/block}

 {block name='page_content_container'}
      <section id="content" class="page-content">
        {block name='page_content'}
          {$variableSmarty1}
        {/block}
      </section>
{/block}