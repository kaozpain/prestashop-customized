<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6b171ea2f737e005ed4c573db24c15eb74f03014fc95684b09f4d86887b929a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d316c85f2f728879f3c98b4b8f29af10a3535b75edfd881c2864acdbcc04cb78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d316c85f2f728879f3c98b4b8f29af10a3535b75edfd881c2864acdbcc04cb78->enter($__internal_d316c85f2f728879f3c98b4b8f29af10a3535b75edfd881c2864acdbcc04cb78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d316c85f2f728879f3c98b4b8f29af10a3535b75edfd881c2864acdbcc04cb78->leave($__internal_d316c85f2f728879f3c98b4b8f29af10a3535b75edfd881c2864acdbcc04cb78_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6d1a9d8622b772b670d56121c72445be1d17c59560903db98d23c316df7eecdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d1a9d8622b772b670d56121c72445be1d17c59560903db98d23c316df7eecdd->enter($__internal_6d1a9d8622b772b670d56121c72445be1d17c59560903db98d23c316df7eecdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_6d1a9d8622b772b670d56121c72445be1d17c59560903db98d23c316df7eecdd->leave($__internal_6d1a9d8622b772b670d56121c72445be1d17c59560903db98d23c316df7eecdd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_720b6546b6e354de2b6cd5267cb9320f7a4f22b9f2876b1c9ba43ed515c7c6f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_720b6546b6e354de2b6cd5267cb9320f7a4f22b9f2876b1c9ba43ed515c7c6f1->enter($__internal_720b6546b6e354de2b6cd5267cb9320f7a4f22b9f2876b1c9ba43ed515c7c6f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_720b6546b6e354de2b6cd5267cb9320f7a4f22b9f2876b1c9ba43ed515c7c6f1->leave($__internal_720b6546b6e354de2b6cd5267cb9320f7a4f22b9f2876b1c9ba43ed515c7c6f1_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e0a3b432e205f11f7275982a6c74f2c6350a3ea013521598d5ba9e61db9341c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0a3b432e205f11f7275982a6c74f2c6350a3ea013521598d5ba9e61db9341c5->enter($__internal_e0a3b432e205f11f7275982a6c74f2c6350a3ea013521598d5ba9e61db9341c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_e0a3b432e205f11f7275982a6c74f2c6350a3ea013521598d5ba9e61db9341c5->leave($__internal_e0a3b432e205f11f7275982a6c74f2c6350a3ea013521598d5ba9e61db9341c5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xamppnew\\htdocs\\tesis\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
