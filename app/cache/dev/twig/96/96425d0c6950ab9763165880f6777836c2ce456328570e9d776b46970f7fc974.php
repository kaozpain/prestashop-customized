<?php

/* ::base.html.twig */
class __TwigTemplate_979399564cff682ea2b73939e9af91007b570a1e441dac592a4f7dd77f7a3280 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b650b07cd3d823e709f35bcc3641d4c731bef18c2f2734457b6c30bbf749fc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b650b07cd3d823e709f35bcc3641d4c731bef18c2f2734457b6c30bbf749fc3->enter($__internal_7b650b07cd3d823e709f35bcc3641d4c731bef18c2f2734457b6c30bbf749fc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 25
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 29
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 30
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 31
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 35
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 36
        echo "    </body>
</html>
";
        
        $__internal_7b650b07cd3d823e709f35bcc3641d4c731bef18c2f2734457b6c30bbf749fc3->leave($__internal_7b650b07cd3d823e709f35bcc3641d4c731bef18c2f2734457b6c30bbf749fc3_prof);

    }

    // line 29
    public function block_title($context, array $blocks = array())
    {
        $__internal_2bce0f392d898da4cfa31945144e64ef5a90afc62f9fe027fec213e9d4d0baf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bce0f392d898da4cfa31945144e64ef5a90afc62f9fe027fec213e9d4d0baf0->enter($__internal_2bce0f392d898da4cfa31945144e64ef5a90afc62f9fe027fec213e9d4d0baf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_2bce0f392d898da4cfa31945144e64ef5a90afc62f9fe027fec213e9d4d0baf0->leave($__internal_2bce0f392d898da4cfa31945144e64ef5a90afc62f9fe027fec213e9d4d0baf0_prof);

    }

    // line 30
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ca427be36cf087b49e19d1a45608b0af0df85f2327789c338e961ec9ceb049ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca427be36cf087b49e19d1a45608b0af0df85f2327789c338e961ec9ceb049ae->enter($__internal_ca427be36cf087b49e19d1a45608b0af0df85f2327789c338e961ec9ceb049ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_ca427be36cf087b49e19d1a45608b0af0df85f2327789c338e961ec9ceb049ae->leave($__internal_ca427be36cf087b49e19d1a45608b0af0df85f2327789c338e961ec9ceb049ae_prof);

    }

    // line 34
    public function block_body($context, array $blocks = array())
    {
        $__internal_60adf5e77aa25d811093d14a7eb28ae11b4c42e6713fbdaf9ea0646c89d1ec1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60adf5e77aa25d811093d14a7eb28ae11b4c42e6713fbdaf9ea0646c89d1ec1e->enter($__internal_60adf5e77aa25d811093d14a7eb28ae11b4c42e6713fbdaf9ea0646c89d1ec1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_60adf5e77aa25d811093d14a7eb28ae11b4c42e6713fbdaf9ea0646c89d1ec1e->leave($__internal_60adf5e77aa25d811093d14a7eb28ae11b4c42e6713fbdaf9ea0646c89d1ec1e_prof);

    }

    // line 35
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f035e142bc4c4ce8ef2fdda9026c27184d4fb7ff824d835c412ff01e347375c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f035e142bc4c4ce8ef2fdda9026c27184d4fb7ff824d835c412ff01e347375c6->enter($__internal_f035e142bc4c4ce8ef2fdda9026c27184d4fb7ff824d835c412ff01e347375c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_f035e142bc4c4ce8ef2fdda9026c27184d4fb7ff824d835c412ff01e347375c6->leave($__internal_f035e142bc4c4ce8ef2fdda9026c27184d4fb7ff824d835c412ff01e347375c6_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 35,  82 => 34,  71 => 30,  59 => 29,  50 => 36,  47 => 35,  45 => 34,  38 => 31,  36 => 30,  32 => 29,  26 => 25,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "C:\\xamppnew\\htdocs\\tesis\\app/Resources\\views/base.html.twig");
    }
}
