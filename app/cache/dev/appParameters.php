<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '3308',
    'database_name' => 'tesis',
    'database_user' => 'root',
    'database_password' => '',
    'database_prefix' => 'tesis_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => '3A0ie9YgIXaruyeEgHxH2YYuWnBG4FNWcQIeOaGTRJywwvunLKrxx3mB',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-05-14',
    'locale' => 'es-ES',
    'cookie_key' => '0Ts25kjjUnxsrgI2zOsNfPhHWXHG5zmPSbudm9eAkb70ULiT2sDwLAA3',
    'cookie_iv' => 'JRxIK0Cc',
    'new_cookie_key' => 'def00000bcf4ad80659eb8fce1ddac284d0d5ca19ac59fe515a5756edbb99532eb534d9528d3dbb90892b8fccfb7350d47426438891716f8d0ee7e8da55a967fb8acd067',
  ),
);
