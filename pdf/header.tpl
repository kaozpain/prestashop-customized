{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}


<table style="width: 100%">
<tr>
	<td style="width: 50%; align-items: center;">
		{if $logo_path}
			<img src="{$logo_path}" style="width:{$width_logo}px; height:{$height_logo}px;" />
		{/if}
	</td>
	<td style="width: 50%; text-align: left; border: all; border-width: thin;">
		<table style="width: 100%">
			<!--
			<tr>
				<td style="font-weight: bold; font-size: 14pt; color: #444; width: 100%;">{if isset($header)}{$header|escape:'html':'UTF-8'|upper}{/if}</td>
			</tr>
			<tr>
				<td style="font-size: 14pt; color: #9E9F9E">{$date|escape:'html':'UTF-8'}</td>
			</tr>
			<tr>
				<td style="font-size: 14pt; color: #9E9F9E">{$title|escape:'html':'UTF-8'}</td>
			</tr>-->
			<!--AM-->
			<tr>
				<td style=" font-size: 8pt; color: #9E9F9E; width: 100%;"><STRONG style="color: #444">RUC: </STRONG>{$shop_details|escape:'html':'UTF-8'|upper}</td>
			</tr>
			<tr>
				<td style="font-weight: bold; font-size: 10pt; color: #444; width: 100%;">{if isset($header)}{$header|escape:'html':'UTF-8'|upper}{/if}</td>
			</tr>
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><STRONG style="color: #444">Nro: </STRONG>{$title|escape:'html':'UTF-8'}</td>
			</tr>			
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Nro de autorizacion: </strong>{$order_invoice_idorder2|escape:'html':'UTF-8'}</td>
			</tr>

			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Fecha de pedido: </strong>{$date|escape:'html':'UTF-8'}</td>
			</tr>


			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Ambiente: </strong>PRODUCCION</td>
			</tr>


			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Emision: </strong>normal</td>
			</tr>


			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Clave de acceso: </strong></td>
			</tr>

			<tr>
			<td>
			<img width="200px" height="20px" src="https://barcode.tec-it.com/barcode.ashx?data={$order_invoice_idorder2}&code=Code128&dpi=72&dataseparator='" alt='Barcode Generator TEC-IT'/></td>
			</tr>
			<!--AM-->
		</table>
	</td>
</tr>
</table>

