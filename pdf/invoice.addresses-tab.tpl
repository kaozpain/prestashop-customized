{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<table id="addresses-tab" cellspacing="1" cellpadding="2">
	<!--Descomentar todo
	<tr>
		<td width="50%">{if $delivery_address}<span class="bold">{l s='Delivery Address' d='Shop.Pdf' pdf='true'}</span><br/><br/>
				{$delivery_address}
			{/if}
		</td>
		<td width="50%"><span class="bold">{l s='Billing Address' d='Shop.Pdf' pdf='true'}</span><br/><br/>
				{$invoice_address}
		</td>
	</tr>-->
	<!--AM-->


	<!--AM-->
<table style="width: 100%">
<tr>
	<td style="width: 50%; text-align: left;" colspan="1">
	<table style="width: 100%">

			<tr>
				<td style="font-weight: bold; font-size: 10pt; color: #444; width: 100%;">{if isset($header)}{$shop_name|escape:'html':'UTF-8'|upper}{/if}</td>
			</tr>

			<tr>
				<td style=" font-size: 8pt; color: #9E9F9E; width: 100%;"><STRONG style="color: #444">Dir. Matriz: </STRONG> {if isset($header)}{$direccion|escape:'html':'UTF-8'|upper}{/if}</td>
			</tr>
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><STRONG style="color: #444">Dir. Establecimiento: </STRONG>{$direccion|escape:'html':'UTF-8'|upper}</td>
			</tr>			
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Contribuyente Especial Nro: </strong></td>
			</tr>

			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Obligado a llevar contabilidad: </strong>SI</td>
			</tr>

			<!--/AM-->
	</table>
	<!--AM-->
	</td>
	<td></td>
</tr>
<tr>
	<td style="width: 100%; text-align: left; border: all; border-width: thin;" colspan="2">
	<table style="width: 100%">
			<tr>
				<td style=" font-size: 8pt; color: #9E9F9E; width: 100%;"><STRONG style="color: #444">Razon Social/ Nombres y Apellidos: </STRONG>{$nombre|escape:'html':'UTF-8'|upper} {$apellido|escape:'html':'UTF-8'|upper}</td>
			</tr>
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><STRONG style="color: #444">Ruc/C.I.: </STRONG>{$cedulacustomer|escape:'html':'UTF-8'}</td>
			</tr>			
			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Fecha Emision: </strong>{$date|escape:'html':'UTF-8'}</td>
			</tr>

			<tr>
				<td style="font-size: 8pt; color: #9E9F9E"><strong style="color: #444">Guia Remision: </strong></td>
			</tr>

			<!--/AM-->
	</table>
	<!--AM-->
	</td>
</tr>
</table>
<!--AM-->
</table>
